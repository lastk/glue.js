var glue = ( function () {
    var modules = {},
        self    = this;

    function Module(name, fn) {
        this.name = name;
        this.fn   = fn;
        this.context = {};
    }

    Module.prototype.load = function() {
        this.fn.call(this.context, this.context, self);
        return this.context;
    }

    function loadModule(name){
        var module = modules[name];
        return module.load();
    }

    define = function(name, fn) {
        modules[name] = new Module(name, fn);
    }

    require = function(name) {
        return loadModule(name).exports;
    }

    return { define: define, require: require }
}).call(this);

/**
   glue.define('components/toolbox', function(module) {
     function ToolBox() {};
     ToolBox.prototype.init = function(){
        //do something
        alert('hi');
     };

   module.exports = ToolBox;
   });

   ToolBox =  glue.require('components/toolbox');
   var toolbox = new ToolBox();
**/
